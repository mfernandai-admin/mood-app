export interface IUser {
    _id: string;
    name: string;
    lastName: string;
    email: string;
    password: string;
    isActive: boolean;
    createdAt?: Date;
    updatedAt?: Date;
  }
  