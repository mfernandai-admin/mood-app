export interface ICampaing {
    name: string;
    description: string;
    createdBy: string;
    startDate: Date;
    endDate: string;
    adminAccess: [
        {
            userId: string;
        }
    ];
    groups: [
        {
            groupId: string;
        }
    ];
    companyId: string;
    isActive: boolean;
  }